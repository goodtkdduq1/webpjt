FROM openjdk:11
ARG JAR_FILE=build/libs/testWebProject-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} testWebProject-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/testWebProject-0.0.1-SNAPSHOT.jar"]
EXPOSE 8080
