# LiveRandomChat - RestAPI1


## Link
[1. 사용 언어](#using)

[2. LocalBuild](#localBuild)

[3. CI/CD](#cicd)

[4. 사용 기술 Blog](#tec)

- [4-1. WebSocker](#WebSocker)
- [4-2. Lombok](#Lombok)
- [4-3. JUNIT5](#JUNIT5)

## 사용 프레임워크 <a id=using></a>
- 언어 : JAVA
- Spring Boot : 2.3.2
- System 구성 : Front-end Rep 참조 ( https://gitlab.com/goodtkdduq1/webpjt_front-end )
- Cloud Server : AWS EC2 우분투 64bit
- Docker


## Build <a id=localBuild></a>
- Gradle
- application.properties 참조


## CI/CD <a id=cicd></a>
- JenkinsFile 참조
- GitLab PR Merge 시 Jenkins Server(Local PC)에서 Trigger를 활성화하여 Jenkin File 실행
- Jenkins File 구성
    1. gradlew clean / build
    2. 기존 Docker Image 삭제
    3. DockerFile의 구성으로 Docker Image 생성
    4. 만들어진 Docker Image로 Container 실행
## 사용 기술 Blog <a id=tec></a>
    
### WebSocker <a id=WebSocker></a>
Web Browser에서 Request를 보내면 Server는 Response를 줍니다.

HTTP 통신의 기본적인 동작 방식입니다.

하지만 Server에서 Client로 특정 동작을 알려야 하는 상황도 있다.

예를 들어 Browser로 Facebook에 접속해 있다가 누군가 친구가 글을 등록하는 경우, 혹은 Web Browser로 메신저를 구현하는 경우가 있습니다.

WebSocket이란 Transport protocol의 일종으로 쉽게 이야기하면 웹버전의 TCP 또는 Socket이라고 이해하면 됩니다.

WebSocket은 서버와 클라이언트 간에 Socket Connection을 유지해서 언제든 양방향 통신 또는 데이터 전송이 가능하도록 하는 기술입니다. 주로

Real-time web application구현을 위해 널리 사용되어지고 있습니다. (SNS 애플리케이션, LoL 같은 멀티플레이어 게임, 구글 Doc, 증권거래, 화상채팅 등)
- Dependencies
```
implementation 'org.springframework.boot:spring-boot-starter-websocket'
```

- Config (main/java/package/config)
``` java
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    // 클라이언트가 메시지를 구독할 endpoint를 정의합니다.
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/webChat/send","/webChat/enterRoomUserInfo","/webChat/exitRoomUserInfo","/webMark/sendMarkdown");
    }

    @Override
    // connection을 맺을때 CORS 허용합니다.
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/webChat").setAllowedOrigins("*").withSockJS();
        registry.addEndpoint("/webMark").setAllowedOrigins("*").withSockJS();
    }

}
```

- Controller
``` java
@Controller
public class WebSocketController {
    // /receive를 메시지를 받을 endpoint로 설정합니다.
    @MessageMapping("/webChat/receive/{roomId}")

    // /send로 메시지를 반환합니다.
    @SendTo("/webChat/send/{roomId}")
    // SocketHandler는 1) /receive에서 메시지를 받고, /send로 메시지를 보내줍니다.
    // 정의한 SocketVO를 1) 인자값, 2) 반환값으로 사용합니다.
    public SocketVO SocketHandler(SocketVO socketVO,@PathVariable String roomId) {
        socketVO.setTime(LocalDateTime.now());          // 전달 받은 Socket Message를 서버 시간으로 설정 후 구독 중인 다른 Client로 전송한다.
        return socketVO;
    }
}
```
### Lombok <a id=Lombok></a>
Lombok이란 Java의 라이브러리로 반복되는 메소드를 Annotation을 사용해서 자동으로 작성해주는 라이브러리입니다. 보통 DTO나 Model, Entity의 경우 여러 속성이 존재하고 이들이 가지는 프로퍼티에 대해서 Getter나 Setter, 생성자 등을 매번 작성해줘야 하는 경우가 많은데 이러한 부분을 자동으로 만들어주는 라이브러리라고 할 수 있습니다.

- @Getter, @Setter
필드들의 Get과 Set을 할 수 있는 메소드들을 자동으로 생성해줍니다.
``` java
import lombok.Getter;
import lombok.Setter;


public class JoinUserVO {
    @Getter
    @Setter
    private String userName;
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PROTECTED)
    private String joinRoom;
    private String joinDate;
}
```
위와 같이 작성 시 userName 필드에 대해 아래와 같은 효과를 얻을 수 있습니다.. 
``` java
public void setUserName(String userName)
{
    this.userName = userName;
}
public String getUserName()
{
    return this.userName;
}
```

기본적으로 public 접근제어로 지정되어 별도로 설정 가능합니다.
``` java
private void setJoinRoom(String joinRoom)
{
    this.joinRoom = joinRoom;
}
protected String getJoinRoom()
{
    return this.joinRoom;
}
```

Class에 명시 시 전체 필드에 대해 적용이 됩니다.
``` java
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JoinUserVO {
    private String userName;
    private String joinRoom;
    private String joinDate;
}
```

- @AllArgsConstructor
 모든 필드에 대한 생성자를 만들어줍니다.
- @NoArgsConstructor
 어떠한 변수도 사용하지 않는 기본 생성자를 자동완성 시켜주는 어노테이션입니다.
- @Data
 @Setter,@Getter를 합쳐놓은 어노테이션입니다.
``` java
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SocketVO {
    private String userName;
    private String content;
    private String joinRoom;
    private boolean isJoin;
    private boolean isExit;
    private String msgDate;
    private LocalDateTime time;
}
```

- @Builder
@Builder 어노테이션을 활용하면 해당 클래스의 객체의 생성에 Builder패턴을 적용시켜줍니다. 모든 변수들에 대해 build하기를 원한다면 클래스 위에 @Builder를 붙이면 되지만, 특정 변수만을 build하기 원한다면 생성자를 작성하고 그 위에 @Builder 어노테이션을 붙여주면 됩니다.
``` java
@Builder
public class SocketVO {
    private String userName;
    private String content;
    private String joinRoom;
    private boolean isJoin;
    private boolean isExit;
    private String msgDate;
    private LocalDateTime time;
}
```
사용법은 아래와 같습니다.
``` java
SocketVO socketVO = SocketVO.builder()
        .joinRoom("1")
        .isExit(true)
        .isJoin(true)
        .content("content")
        .msgDate("2022")
        .userName("userName")
        .time(LocalDateTime.now())
        .build();
```
### JUNIT5 <a id=JUNIT5></a>
JUNIT5 기술은 두번째 RestAPI ReadMe 참조.(https://gitlab.com/goodtkdduq1/webpjt_rest_second)


