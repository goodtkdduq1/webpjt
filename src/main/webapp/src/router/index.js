import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import FirstPage from '@/components/firstPage/firstPage.vue'

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      redirect: '/chat', // 초기 진입 페이지 설정
    },
    {
      path: '/sec',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path:'/chat',
      name:'firstPage',
      component : FirstPage
    }
  ]
})
