package com.web.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class CodeSocketDto {
    private String mark;
    private String content;
    private String userName;
    private LocalDateTime time;
}
