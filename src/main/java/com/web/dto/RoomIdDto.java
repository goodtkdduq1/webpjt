package com.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomIdDto {
    private String RoomId;
    private String whom;

}
