package com.web.dto;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SocketDto {
    private String userName;
    private String content;
    private String joinRoom;
    private boolean isJoin;
    private boolean isExit;
    private String msgDate;
    private LocalDateTime time;

}
