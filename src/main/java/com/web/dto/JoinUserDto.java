package com.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JoinUserDto {
    private String userName;
    private String joinRoom;
    private String joinDate;
}
