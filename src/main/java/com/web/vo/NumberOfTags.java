package com.web.vo;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NumberOfTags {
    private String tag;
    private int size;
}
