package com.web.vo;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
public class UserListByRoom {
    String roomId;
    ArrayList<String> users = new ArrayList<>();

    public void setUser(String user){
        users.add(user);
    }

}
