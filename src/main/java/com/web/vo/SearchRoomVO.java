package com.web.vo;

import com.web.service.ChatManager;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class SearchRoomVO {
    private String userName;
    private String whom;
    private ArrayList<String> tags;
    private String roomId;

    public int getRoomId(ChatManager chatManager){
        int returnId=0;

        if (tags.size()>0){
            for(String key:tags) {
                if (whom.equals("answer")) {
                    returnId = chatManager.checkAnswerHashID(key);
                } else {
                    returnId = chatManager.checkQuestionHashID(key);
                }
            }
        }else {
            if (whom.equals("answer")) {
                returnId = chatManager.checkAnswerID();
            } else {
                returnId = chatManager.checkQuestionID();
            }
        }
        chatManager.plusChatRoomdId();
        printTags(chatManager);
        chatManager.printAllAnswerHash();

        return returnId;
    }

    public void removeRoomId(ChatManager chatManager){
        if (tags.size()>0){
            for(String key:tags){
                if (whom.equals("answer")) {
                    chatManager.removeAnswerHash(roomId,key);
                }else{
                    chatManager.removeQuestionHash(roomId,key);
                }
            }
        }
        else{
            if (whom.equals("answer")) {
                chatManager.removeAnswerQueue(roomId);
            }else{
                chatManager.removeQuestionQueue(roomId);
            }
        }
    }

    private void printTags(ChatManager chatManager){
        System.out.println("-- Tag size: "+tags.size());
        for(String item : chatManager.getAnswerHash().keySet()){
            System.out.println("-"+item);
            for(Integer room:chatManager.getAnswerHash().get(item)){
                System.out.println("--"+room);
            }

        }
    }
}
