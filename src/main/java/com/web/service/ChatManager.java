package com.web.service;

import com.web.exception.RoomNotFoundException;
import com.web.exception.TagNotFoundException;
import com.web.vo.AnnoymousNumberVO;
import com.web.vo.NumberOfTags;
import com.web.vo.UserListByRoom;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.lang.Integer.parseInt;

@Data
@Service
public class ChatManager {
    private HashMap<String,Queue<Integer>> answerHash = new HashMap<>();
    private HashMap<String,Queue<Integer>> questionHash = new HashMap<>();
    private Queue<Integer> answerQueue = new LinkedList<>();
    private Queue<Integer> questionQueue = new LinkedList<>();

    private HashMap<Integer, ArrayList<String>> tagsByRoomId = new HashMap<>();
    int chatRoomId=0;
    int anonymousUser=0;
    private HashMap<String, UserListByRoom> usersByRoomId = new HashMap<>();


    public synchronized int annoyUserCount(){
        return this.anonymousUser++;
    }

    public synchronized void setUserByRoomId(String roomId,String user)
    {
        if(!usersByRoomId.containsKey(roomId))
            usersByRoomId.put(roomId,new UserListByRoom());
        usersByRoomId.get(roomId).setRoomId(roomId);
        usersByRoomId.get(roomId).setUser(user);
    }

    public synchronized UserListByRoom getUserByRoomId(String roomId)
    {

        if(usersByRoomId.containsKey(roomId))
        {
            return usersByRoomId.get(roomId);
        }else{
            throw new RoomNotFoundException();
        }
    }

    public synchronized List<NumberOfTags> answerNumberOfTags(){
        List<NumberOfTags> temp = new LinkedList<>();
        for (String tag:answerHash.keySet()) {
            NumberOfTags numTags = new NumberOfTags();
            numTags.setTag(tag);
            numTags.setSize(answerHash.get(tag).size());
            temp.add(numTags);
        }
        return temp;
    }

    public synchronized List<NumberOfTags> questionNumberOfTags(){
        List<NumberOfTags> temp = new LinkedList<>();
        for (String tag:questionHash.keySet()) {
            NumberOfTags numTags = new NumberOfTags();
            numTags.setTag(tag);
            numTags.setSize(questionHash.get(tag).size());
            temp.add(numTags);
        }

        return temp;
    }

    public synchronized void plusChatRoomdId(){
        this.chatRoomId++;
    }
    public Queue<Integer> makeValueInit(){
        Queue<Integer> tempQueue = new LinkedList<>();
        return tempQueue;
    }
    public synchronized int addAnswerHash(String key){
        if(!answerHash.containsKey(key)){
            answerHash.put(key,makeValueInit());
        }
        if(!tagsByRoomId.containsKey(chatRoomId))
            tagsByRoomId.put(chatRoomId,new ArrayList<>());

        tagsByRoomId.get(chatRoomId).add(key);
        answerHash.get(key).add(chatRoomId);
        return chatRoomId;
    }

    public synchronized int addQuestionHash(String key)
    {
        if(!questionHash.containsKey(key)){
            questionHash.put(key,makeValueInit());
        }

        if(!tagsByRoomId.containsKey(chatRoomId))
            tagsByRoomId.put(chatRoomId,new ArrayList<>());

        tagsByRoomId.get(chatRoomId).add(key);
        questionHash.get(key).add(chatRoomId);
        return chatRoomId;
    }


    public synchronized void removeRemainHash(int id,boolean isAnswer,String currentKey){
        for(String key :tagsByRoomId.get(id)){
            if (!key.equals(currentKey)) {
                if (isAnswer) {
                    answerHash.get(key).remove(id);
                } else {
                    questionHash.get(key).remove(id);
                }
            }
        }
    }
    public synchronized int checkQuestionHashID(String key){
        if(answerHash.containsKey(key)){
            if (answerHash.get(key).size()>0){
                removeRemainHash(answerHash.get(key).peek(),true,key);
                return answerHash.get(key).poll();
            }else{
                return addQuestionHash(key);
            }
        }else{
            return addQuestionHash(key);
        }
    }

    public synchronized int checkAnswerHashID(String key){
        if(questionHash.containsKey(key)){
            if (!questionHash.get(key).isEmpty()){
                removeRemainHash(questionHash.get(key).peek(),false,key);
                return questionHash.get(key).poll();
            }else{
                return addAnswerHash(key);
            }
        }else{
            return addAnswerHash(key);
        }
    }

    public synchronized void removeAnswerHash(String roomId,String key){
        answerTagVaildation(key);
        answerHash.get(key).remove(parseInt(roomId));
    }

    public synchronized void removeQuestionHash(String roomId,String key){
        questionTagVaildation(key);
        questionHash.get(key).remove(parseInt(roomId));
    }

    public synchronized void removeAnswerQueue(String roomId){
        answerRoomValidation(roomId);
        answerQueue.remove(parseInt(roomId));
    }

    public synchronized void removeQuestionQueue(String roomId){
        questionRoomValidation(roomId);
        questionQueue.remove(parseInt(roomId));
    }

    private synchronized int addAnswerQueue(){
        answerQueue.add(chatRoomId);
        return chatRoomId;
    }

    private synchronized int addQuestionQueue(){
        questionQueue.add(chatRoomId);
        return chatRoomId;
    }

    public synchronized int checkQuestionID(){
        if(answerQueue.size()>0){
            return answerQueue.poll();
        }
        else{
            return addQuestionQueue();
        }
    }

    public synchronized int checkAnswerID(){
        if(questionQueue.size()>0){
            return questionQueue.poll();
        }else{
            return addAnswerQueue();
        }
    }


    public void printAllAnswerHash(){
        for(String key:answerHash.keySet()){
            System.out.println(key);
        }
    }

    public synchronized String removeRoomByRoomId(String roomId)
    {
        questionQueue.remove(parseInt(roomId));
        for(int i =0;i<tagsByRoomId.get(parseInt(roomId)).size();i++)
        {
            removeQuestionHash(roomId,tagsByRoomId.get(parseInt(roomId)).get(i));
        }

        return roomId;
    }

    public AnnoymousNumberVO getAnnoymousNumber(){
        AnnoymousNumberVO annoymousNumberVO = new AnnoymousNumberVO();
        annoymousNumberVO.setAnnUserId(annoyUserCount());
        return annoymousNumberVO;
    }

    private void answerTagVaildation(String key){
        if(!answerHash.containsKey(key))
            throw new TagNotFoundException();
    }

    private void questionTagVaildation(String key){
        if(!questionHash.containsKey(key))
            throw new TagNotFoundException();
    }

    private void answerRoomValidation(String roomId){
        if(!answerQueue.contains(parseInt(roomId)))
            throw new RoomNotFoundException();
    }

    private void questionRoomValidation(String roomId){
        if(!questionQueue.contains(parseInt(roomId)))
            throw new RoomNotFoundException();
    }
}
