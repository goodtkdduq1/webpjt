package com.web.contorller;

import com.web.dto.CodeSocketDto;
import com.web.dto.SocketDto;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Controller
public class WebSocketController {
    // /receive를 메시지를 받을 endpoint로 설정합니다.
    @MessageMapping("/webChat/receive/{roomId}")

    // /send로 메시지를 반환합니다.
    @SendTo("/webChat/send/{roomId}")
    // SocketHandler는 1) /receive에서 메시지를 받고, /send로 메시지를 보내줍니다.
    // 정의한 SocketVO를 1) 인자값, 2) 반환값으로 사용합니다.
    public SocketDto SocketHandler(SocketDto socketVO, @PathVariable String roomId) {
        // vo에서 getter로 userName을 가져옵니다.
        String userName = socketVO.getUserName();
        // vo에서 setter로 content를 가져옵니다.
        String content = socketVO.getContent();
        System.out.println(userName+" : "+content);
        //System.out.println(socketVO.getTags());
        // 생성자로 반환값을 생성합니다.
        ArrayList<String> tempTags = new ArrayList<String>();
        //SocketVO result = new SocketVO(userName, content,roomId,false,"2020",tempTags);
        // socketVo 내 Local Time 추가
        socketVO.setTime(LocalDateTime.now());
        return socketVO;
    }


    @MessageMapping("/webChat/joinUser/{roomId}")
    @SendTo("/webChat/enterRoomUserInfo/{roomId}")
    public SocketDto enterSend(SocketDto secketVO, @PathVariable String roomId){
        System.out.println("enterSend 호출 ");
        secketVO.setJoin(true);
        return secketVO;
    }

    @MessageMapping("/webChat/exitUser/{roomId}")
    @SendTo("/webChat/exitRoomUserInfo/{roomId}")
    public SocketDto exitSend(SocketDto secketVO, @PathVariable String roomId){
        System.out.println("exitSend 호출 ");
        secketVO.setJoin(true);
        return secketVO;
    }

    @MessageMapping("/webMark/receiveMarkdown/{roomId}")
    // /send로 메시지를 반환합니다.
    @SendTo("/webMark/sendMarkdown/{roomId}")
    // SocketHandler는 1) /receive에서 메시지를 받고, /send로 메시지를 보내줍니다.
    // 정의한 SocketVO를 1) 인자값, 2) 반환값으로 사용합니다.
    public CodeSocketDto SocketHandlerMarkdown(CodeSocketDto codeSocketDto, @PathVariable String roomId) {
        // vo에서 getter로 userName을 가져옵니다.
        String userName = codeSocketDto.getUserName();
        // vo에서 setter로 content를 가져옵니다.
        String content = codeSocketDto.getContent();
        String mark = codeSocketDto.getMark();
        System.out.println(userName + " : " + content + " , "+mark);
        codeSocketDto.setTime(LocalDateTime.now());
        return codeSocketDto;
    }
}


