package com.web.contorller;

import com.web.dto.RoomIdDto;
import com.web.vo.SearchRoomVO;
import com.web.service.ChatManager;
import com.web.vo.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class BasicRestController {
    private final ChatManager chatManager;

    @RequestMapping(value="/searchRoomId",method= RequestMethod.POST)
    public RoomIdDto searchRoomId(@RequestBody SearchRoomVO searchRoomVO){
        RoomIdDto ret = new RoomIdDto();
        ret.setRoomId(Integer.toString(searchRoomVO.getRoomId(chatManager)));
        return ret;
    }

    @RequestMapping(value="/exit", method=RequestMethod.POST)
    public SearchRoomVO exitRoomId(@RequestBody SearchRoomVO searchRoomVO){
        searchRoomVO.removeRoomId(chatManager);
        return searchRoomVO;
    }

    @RequestMapping(value="/exit/{roomId}", method=RequestMethod.DELETE)
    public String exitRoomId(@PathVariable String roomId){
        return chatManager.removeRoomByRoomId(roomId);
    }

    @RequestMapping("/answer/labelList")
    public List<NumberOfTags> answerLabelList(){
        return chatManager.answerNumberOfTags();
    }


    @RequestMapping("/question/labelList")
    public List<NumberOfTags> questionLabelList(){
        return chatManager.questionNumberOfTags();
    }

    @RequestMapping("/anonymousUser")
    public AnnoymousNumberVO anonymousUser(){
        return chatManager.getAnnoymousNumber();
    }

    @RequestMapping("/enterUsers/{roomId}")
    public UserListByRoom enterUsers(@PathVariable String roomId){
        return chatManager.getUserByRoomId(roomId);
    }

    @RequestMapping("/enterUsersInfo/{roomId}/{user}")
    public UserListByRoom enterUsersInfo(@PathVariable String roomId,@PathVariable String user){
        chatManager.setUserByRoomId(roomId,user);
        return chatManager.getUserByRoomId(roomId);
    }

}
