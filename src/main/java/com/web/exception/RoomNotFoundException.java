package com.web.exception;

public class RoomNotFoundException extends CommonException {
    public RoomNotFoundException(){
        super(ExceptionCode.ROOM_NOT_FOUND_EXCEPTION);
    }
}
