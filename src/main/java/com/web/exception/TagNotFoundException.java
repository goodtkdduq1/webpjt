package com.web.exception;

public class TagNotFoundException extends CommonException{
    public TagNotFoundException(){
        super(ExceptionCode.TAG_NOT_FOUND_EXCEPTION);
    }
}
