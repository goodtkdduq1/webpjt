package com.web.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ExceptionCode implements CommonExceptionCode {
    ROOM_NOT_FOUND_EXCEPTION(400,"ROOM-1","ROOM을 찾을 수 없습니다."),
    ROOM_CAN_NOT_REMOVE_EXCEPTION(400,"ROOM-2","ROOM을 삭제 할 수 없습니다."),
    TAG_NOT_FOUND_EXCEPTION(400,"TAG-1","Tag를 찾을 수 없습니다.")
    ;

    private int status;
    private String errorCode;
    private String message;

}
