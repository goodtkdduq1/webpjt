package com.web.exception;


public interface CommonExceptionCode {
    int getStatus();
    String getErrorCode();
    String getMessage();

}
