package com.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CommonExceptionHandler {
    @ExceptionHandler(RoomNotFoundException.class)
    public ResponseEntity<Object> handleCustomException(RoomNotFoundException e){
        return ResponseEntity.status(e.getExceptionCode().getStatus()).body(ExceptionResponse.builder()
                .status(e.getExceptionCode().getStatus())
                .code(e.getExceptionCode().getErrorCode())
                .message(e.getExceptionCode().getMessage())
                .build());
    }

}
