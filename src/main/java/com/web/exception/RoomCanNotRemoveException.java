package com.web.exception;

public class RoomCanNotRemoveException extends CommonException{

    public RoomCanNotRemoveException() {
        super(ExceptionCode.ROOM_CAN_NOT_REMOVE_EXCEPTION);
    }
}
