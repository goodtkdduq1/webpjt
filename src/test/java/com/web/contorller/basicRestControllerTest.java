package com.web.contorller;

import com.web.dto.RoomIdDto;
import com.web.exception.ExceptionResponse;
import com.web.vo.SearchRoomVO;
import com.web.vo.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;


@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)
class basicRestControllerTest {
    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    @DisplayName("searchRoomId Test Case 1 - insert answer User")
    public void searchRoomId_test_1() {
        String UserName = "testUser1";
        String whom = "answer";
        SearchRoomVO searchRoomVO = new SearchRoomVO();
        searchRoomVO.setUserName(UserName);
        searchRoomVO.setWhom(whom);
        searchRoomVO.setTags(new ArrayList<>());
        RoomIdDto result = testRestTemplate.postForObject("/searchRoomId",searchRoomVO, RoomIdDto.class);
        assertEquals(result.getRoomId(),"0");
        UserListByRoom result2 = testRestTemplate.getForObject("/enterUsersInfo/"+result.getRoomId()+"/"+UserName, UserListByRoom.class);

        UserListByRoom excpect = new UserListByRoom();
        excpect.setRoomId("0");
        excpect.setUser(UserName);
        Assertions.assertEquals(result2.getRoomId(), excpect.getRoomId());

    }

    @Test
    @DisplayName("searchRoomId Test Case 2 - matching Question User")
    public void searchRoomId_test_2() {
        String UserName = "testUser2";
        String whom = "question";
        SearchRoomVO searchRoomVO = new SearchRoomVO();
        searchRoomVO.setUserName(UserName);
        searchRoomVO.setWhom(whom);
        searchRoomVO.setTags(new ArrayList<>());
        RoomIdDto result = testRestTemplate.postForObject("/searchRoomId",searchRoomVO, RoomIdDto.class);
        assertEquals(result.getRoomId(),"0");
        UserListByRoom result2 = testRestTemplate.getForObject("/enterUsersInfo/"+result.getRoomId()+"/"+UserName, UserListByRoom.class);

        UserListByRoom expect = new UserListByRoom();
        expect.setRoomId("0");
        expect.setUser("testUser1");       // testcase1에서 추가한 User
        expect.setUser(UserName);
        assertEquals(result2.getRoomId(),expect.getRoomId());

        UserListByRoom result3 = testRestTemplate.getForObject("/enterUsers/0", UserListByRoom.class);
        UserListByRoom expect3 = new UserListByRoom();
        expect3.setRoomId("0");
        expect3.setUser("testUser1");       // testcase1에서 추가한 User
        expect3.setUser("testUser2");
        assertEquals(result3.getUsers().get(0),expect3.getUsers().get(0));
    }

    @Test
    @DisplayName("searchRoomId Test Case 3 - answer User First")
    public void searchRoomId_test_3(){
        String UserName = "testUser3";
        String whom = "question";
        SearchRoomVO searchRoomVO = new SearchRoomVO();
        searchRoomVO.setUserName(UserName);
        searchRoomVO.setWhom(whom);
        searchRoomVO.setTags(new ArrayList<>());
        RoomIdDto result = testRestTemplate.postForObject("/searchRoomId",searchRoomVO, RoomIdDto.class);
        assertEquals(result.getRoomId(),"2");
        UserListByRoom result2 = testRestTemplate.getForObject("/enterUsersInfo/"+result.getRoomId()+"/"+UserName, UserListByRoom.class);

        UserListByRoom excpect = new UserListByRoom();
        excpect.setRoomId("2");
        excpect.setUser(UserName);
        assertEquals(result2.getUsers().get(0),excpect.getUsers().get(0));
    }

    @Test
    @DisplayName("searchRoomId Test Case 4 - matching question")
    public void searchRoomId_test_4(){
        String UserName = "testUser4";
        String whom = "answer";
        SearchRoomVO searchRoomVO = new SearchRoomVO();
        searchRoomVO.setUserName(UserName);
        searchRoomVO.setWhom(whom);
        searchRoomVO.setTags(new ArrayList<>());
        RoomIdDto result = testRestTemplate.postForObject("/searchRoomId",searchRoomVO, RoomIdDto.class);
        assertEquals(result.getRoomId(),"2");
        UserListByRoom result2 = testRestTemplate.getForObject("/enterUsersInfo/"+result.getRoomId()+"/"+UserName, UserListByRoom.class);

        UserListByRoom excpect = new UserListByRoom();
        excpect.setRoomId("2");
        excpect.setUser("testUser3");
        excpect.setUser(UserName);
        assertEquals(result2.getUsers().get(1),excpect.getUsers().get(1));

        UserListByRoom result3 = testRestTemplate.getForObject("/enterUsers/2", UserListByRoom.class);
        UserListByRoom expect3 = new UserListByRoom();
        expect3.setRoomId("2");
        expect3.setUser("testUser3");       // testcase1에서 추가한 User
        expect3.setUser("testUser4");
        assertEquals(result3.getUsers().get(1),expect3.getUsers().get(1));
    }

    @Test
    @DisplayName("searchRoomId with Tags Test Case 1 - answer Tags")
    public void searchRoomId_test_with_tags_1(){
        String UserName = "testUser6";
        String whom = "answer";
        SearchRoomVO searchRoomVO = new SearchRoomVO();
        searchRoomVO.setUserName(UserName);
        searchRoomVO.setWhom(whom);
        ArrayList<String> tags = new ArrayList<>();
        tags.add("testTags1");
        searchRoomVO.setTags(tags);
        RoomIdDto result = testRestTemplate.postForObject("/searchRoomId",searchRoomVO, RoomIdDto.class);
        assertEquals(result.getRoomId(),"4");

    }


    @Test
    @DisplayName("searchRoomId with Tags Test Case 2 - matching question Tags")
    public void searchRoomId_test_with_tags_2(){
        String UserName = "testUser7";
        String whom = "question";
        SearchRoomVO searchRoomVO = new SearchRoomVO();
        searchRoomVO.setUserName(UserName);
        searchRoomVO.setWhom(whom);
        ArrayList<String> tags = new ArrayList<>();
        tags.add("testTags1");
        searchRoomVO.setTags(tags);
        RoomIdDto result = testRestTemplate.postForObject("/searchRoomId",searchRoomVO, RoomIdDto.class);
        assertEquals(result.getRoomId(),"4");

    }

    @Test
    @DisplayName("searchRoomId with Tags Test Case 3 - question Tags")
    public void searchRoomId_test_with_tags_3(){
        String UserName = "testUser8";
        String whom = "question";
        SearchRoomVO searchRoomVO = new SearchRoomVO();
        searchRoomVO.setUserName(UserName);
        searchRoomVO.setWhom(whom);
        ArrayList<String> tags = new ArrayList<>();
        tags.add("testTags2");
        searchRoomVO.setTags(tags);
        RoomIdDto result = testRestTemplate.postForObject("/searchRoomId",searchRoomVO, RoomIdDto.class);
        assertEquals(result.getRoomId(),"6");
    }

    @Test
    @DisplayName("searchRoomId with Tags Test Case 4 - matching answer Tags")
    public void searchRoomId_test_with_tags_4(){
        String UserName = "testUser9";
        String whom = "answer";
        SearchRoomVO searchRoomVO = new SearchRoomVO();
        searchRoomVO.setUserName(UserName);
        searchRoomVO.setWhom(whom);
        ArrayList<String> tags = new ArrayList<>();
        tags.add("testTags2");
        searchRoomVO.setTags(tags);
        RoomIdDto result = testRestTemplate.postForObject("/searchRoomId",searchRoomVO, RoomIdDto.class);
        assertEquals(result.getRoomId(),"6");

    }

    @Test
    @DisplayName("anonymousUser Test Case 1")
    public void anonymousUser_test_1(){
        AnnoymousNumberVO result = testRestTemplate.getForObject("/anonymousUser", AnnoymousNumberVO.class);
        assertEquals(result.getAnnUserId(),0);
    }

    @Test
    @DisplayName("anonymousUser Test Case 2")
    public void anonymousUser_test_2(){
        AnnoymousNumberVO result = testRestTemplate.getForObject("/anonymousUser", AnnoymousNumberVO.class);
        assertEquals(result.getAnnUserId(),1);
    }

    @Test
    @DisplayName("Room Not Found Test")
    public void roomNotFoundTest(){
        ExceptionResponse result = testRestTemplate.getForObject("/enterUsers/1", ExceptionResponse.class);
        assertEquals(result.getCode(),"ROOM-1");
    }

}